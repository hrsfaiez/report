***Spécification des besoins***

Dans ce chapitre, nous présentons une vue globale sur les besoins des utilisateurs. Cette vue englobe les problèmes que nous souhaitons résoudre. Cela nous aidera, par suite, à comprendre le contexte de chaque incrément et à avoir un point de départ pour organiser les fonctionnalités selon leurs valeurs relatives. Nous définissons, par suite, la méthode de décomposition de ces fonctionalités en des 'User story'.

**Spécification fonctionnelle**

**Spécification non fonctionnelle**

Les besoins non fonctionnels de notre eapplication sont:

- Sécurité: Aucun utilisateur du système ne peut s'authentifier à la place d'un autre un autre
- Performance: L'ajout d'une transaction(chagement d'état du système) doit être minimiser
- Journalisation: Tous les interactions de l'application avec le milieur extrenes doivent être sauvegardées avec leurs détails repsectives
- Ergonomie: L'application doit être facile à utiliser pour un utilisateur avec une connaissance du métier

**Acteurs**

Les utilisateurs de notre systèmes sont les suivants:

- Le distributeur
- L'initiateur du fond
- Le souscripteur

**Backlog du porduit**

Nous utilisons une méthodologie itérative et incrémentale. D'où les besoins aussi que leurs priorités changent. On est besoin, en outre, d'une vue globale qui trace le contexte de l'application et les problèmes majeurs qu'elle addresse.

Le Backlog du produit contient les fonctionnalités majeurs de l'application. Nous décomposerons ces derniers en des 'User stories' dans les sessions de plannifications à chaque début d'itération.

Le Backlog du produit contient

- Le distributeur demande une opportunité dans un fond
- L'initiateur de fond accepte l'opportunité d'un distributeur
- L'initiateur de fond peut transmettre la possession d'un fond à un autre utilisateur du système
- Le distributeur de fond peut transmettre la possession d'une opportunité à un autre utilisateur du système
- L'initiateur de fond change le status d'une opportunité
- L'initiateur de fond change les détails d'une opportunité de son fond
- Le distributeur change les détails de son opportunité
- Le distributeur gagne un part dans un fond
- Le souscripteur demande une opportunité dans un part
- Le distributeur accepte la demande d'un souscripteur
- Le distribteur gagne des bien à chaque validation d'un part à un souscripteur

**Décomposition en 'User stories'**

Dans chaque sesssion de plannification, et en se basant sur cette liste, on décompse les fonctionalités les plus prioritaires. Puis, nous choisissons, en se basant sur l'avis de l'expert métier, les 'User stories' que nous travaillerons sur durant l'itération.
Nous laissons les autres 'User stories' pour les itérations suivantes.

*Card-Conversation-Confirmation*

Pour spécifier les scénarios que nous implémenterons, s'assurer que les 'User stories' choisis ont une valeur ajoutée, et valider les étapes des scénarios du point de vue de l'utilisateur final, nous utiliserons la méthode 'Card-Conversation-Confirmation' pour la spécification du comportant attendu du système pour chaque 'User story'.

*Card*
"User stories are written on cards. The card does not contain all the information that makes up the requirement" -Ron Jeffries, his web site

Pour chaque 'User story', on associe une carte sur un tableau éléctronique. Cette cart passe du Backlog du produit vers le Backlog de l'itération le moment qu'elle est sélectionné pour l'implémentation.

*Conversation*

"The requirement itself is communicated from customer through conversation"

Lorsqu'un 'User story' est sélectionné pour l'implémentation, on disctue sa valeur ajoutée et les termes métier nécessaires pour l'implélenter.


*Confirmation*

"At the beginning of the iteration, the customer communicates to the what she wants, by telling them how she will confirm that they’ve done what is needed."

Pour confirmer les scénarios de chaque 'User stories' et pour avoir un tôt feedback sur les étapes, nous préparons des maquettes pour chaque étape de chaque scénario. Nous présentons ces derniers à l'utilisateurs pour la validation.
Nous ne commencerons l'implémentation d'un scénario que s'il est validé.

