***Itération Une(Un 'Walking Skeleton')***

Nous présentons dans ce chapitre le déroulement de la permière itération. Dans laquelle, on prépare un environnement de travail pour simplifier l'implémentation des fonctionnalités suivantes et on vérifie l'aptitude des outils utilisés.

***Spécification***

L'objectif de cette itération est d'avoir un système avec des fonctionnalités basiques.

Le Backlog itération contient les 'User stories' suivantes:

- Un initiateur crée un fond
- Un distributeur demande une opportunité dans un fond
- Un initiateur de fond crée un type de parts

Pour chaqu'une de ces 'User stoies', nous définissons les scénarios d'utilisation (i.e. un scénario parfait, appelé aussi, le 'Happy path', et les scénarios qui présentent des situations des erreurs).

***Coneception***

- Digramme de séquence
  - Le scénario
- Diagramme de composants
- Diagramme de modules


*Maquettes*

Nous essayons de guarder les mêmes étapes du scénario que l'application existente.
Les utilisateurs actuels connaissent bien atteindre leurs objectifs, un changment dans les étapes d'un scénario peut lever une confusion.

Pour demander une opportunité dans un fond, le distributeur passe par les étapes suivantes:

...


***Réalisation***

**Technologie Blockchain**

*Comparison*



*Choix*

Chaque transaction dans le Blockchain prends le système d'un état à un autre.
Une transaction, donc, présente une état-transition.
Une transaction n'est ajoutée à la chaine que si elle est validée par tous les utilisateurs du système. Les utilisateurs appliquent l'état-transition associé à cette transaction et s'ils obtiennent le même état final. Chaqu'un accepte cette transaction. D'où un block n'est ajouté à la chaine que si tous ses transactions sont validées par tous les utilisateurs du systèmes.

Il fallait avoir un algorithme qui, à partir d'un état initial et une transaction, décide l'état suivant.
Il existe des systèmes Blockchain oú cet algorithme est définit lors de la création de la chaîne. C'est le cas des systèmes d'échange du monnaie digital ou des biens bien définits initialement. Chaque transaction, dans ces systèmes, vérifie que la source a la somme à transférer, augmente la somme du compte du distinataire et diminue la même somme de la compte source.

Un autre type des systèmes exisye oú cet algorithme, lui même, est publié sur la chaine.

Nous optons pour le premier type dans notre application. En effet, la publication d'un programme sur la chaine présente un risque?? si le programme est erroné et pose des problèmes lors de la mise à jours du programme.

La technolgie qui utiliserons est Multichain.

"MultiChain is an off­the­shelf platform for the creation and deployment of private blockchains, either  within or between organizations. It aims to overcome a key obstacle to the deployment of blockchain  technology in the institutional financial sector, by providing the privacy and control required in an  easy­to­use package."--Multichain white paper

**Exploitation du Blockchain**

*Event sourcing*

"Event Sourcing ensures that all changes to application state are stored as a sequence of events. Not just can we query these events, we can also use the event log to reconstruct past states, and as a foundation to automatically adjust the state to cope with retroactive changes."
-- Martin Fowler, https://martinfowler.com/eaaDev/EventSourcing.html

*Intégration avec le Blockchain*

Nous voiyons chaque transaction comme un évènement qui nécessite l'authentification du source et du destinataire. Chaque évènement dans la chaine présente le transfert de la possession d'un bien d'un utilisateur à un autre (e.g. un part d'un fond, une opportunité de contribution dans un fond, une augmentation dans le chance d'un part, ...)

Ainsi, la collection de tous les évènements(les transactions) donne l'état actuel de la chaine.

Chaque asset dans notre système présente un bien et chaque transfer d'asset modèle un évènement du monde réel.
Ce sont les évènements qui définie les assets. Pour, chaque nouveau type d'évènement introduit, on révise la liste assets utilisés les contraintes d'échange de ces assets.

Nous commençons, dans notre cas, par le scénario optimiste du 'User story' "Un initiateur crée un fond".
Les étapes de ce scénario sont les suivantes:

- L'initateur introduit les données d'un fonds(Identité, nom, date d'expiration, taille, entreprises inclus)
- L'initiateur envoie le données vers la chaine
- La chaine valide que l'identité globale saisie du fond est unique
- La chaine met le fond dans un block
- Tous les utilisateurs du système peuvent demander des opportunité dans ce fond

**Autres Technologies utilisées**

- Figma
- Angular 2 et ses dépendances
- Express js
- Node js
- NPM (Node Package Manager)
- Jasmine
- Karma et ses plugins
- make
- Git
- Pug
- Scss
- Trello

**Walking Skeleton**

Nous commençons par un 'Walking Skeleton'.

'A **Walking Skeleton** is a tiny implementation of the system that performs a small end-to-end function. It need not use the final architecture, but it should link together the main architectural components. The architecture and the functionality can then evolve in parallel.

...

Note that each subsystem is incomplete, but they are hooked together, and will stay hooked together from this point on.'
-- Alistair Cockburn, http://alistair.cockburn.us/Walking+skeleton

Comme étape initiale, nous travaillons sur un scénario simple, mais, qui offre une valeur ajoutée pour un utilisateur final.
Le résultat de cette itération est un incrément qui assure la faisabilité de notre application et qui donne un point de sécurité pour se concenter sur l'ajouter de nouvelles fonctionalités et raffiner la conception sans s'occupper de problèmes de la technologie et de l'infrastructure.

L'implémentation de cet incrément comprend toutes les technologies utilisées dans le projet (e.g. Les frameworks, la technologie Blockchain, les transpliteurs, les outils de build, ...).
Elle assure que les technologies peuvent être utilisé ensemble. Aussi, ça nous permet de fixer les technologies à utiliser et automitiser des parties majeures des processus de 'release', test, et déploiement.

Cela mène à éxtraire les composants majeurs de l'application et de les relier en offrants un incrément de valeur.

Cette itération aussi nous aide à essayer le processus choisie et trouver des améliorations.

**Processus automatique de 'release'**

Nous essayons dans cette itération de préparer un processus de 'release' initial.
Pour le client, on passe par:

- Transpilation du code de l'application Typescript vers du JavaScript
- Transpilation et éxécution des tests unitaires
- Compilation du Pug vers du HTML et du SCSS vers du CSS
- Transpilation et éxécution des tests d'intégration
- Lancer le serveur et lancer des tests de contrats
- Lancement de l'application et éxécuter des smoke tests pour s'assurer du fonctionnement des éléments importants
- Lancement des tests de contract pour s'assurer que le comportement du serveur est celui attendu



Pour le serveur, on passe par:

- Transpilation du code de l'application Typescript vers du JavaScript
- Transpilation et éxécution des tests unitaires
- Transpilation et éxécution des tests d'intégration
- Lancement de l'application et éxécuter des smoke tests pour s'assurer du fonctionnement des éléments importants