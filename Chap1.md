**Présentation de l'entreprise**

...

**Contexte**

La gestion des actifs(autrement, gestion de portefeuilles) est l'ensemble de systèmes qui gèrent des fonds confiés par des investisseurs dans le but de générer un revenu. Les fonds, souvent, présentent des contributions dans des entreprises.

C'est un domaine riche d'opportunités; il est atteignable pour des acteurs de différentes puissances économiques et il connaît un progrès accéléré grâce à la naissance de petites entreprises et de start-up.

Le monde de capital-développement (Private Equity), comme une branche de la gestion des actifs, se concentre sur la gestion des parts privés dans des fonds. Un part privé, contrairement à un part public, n'est pas par déclarer en bourse.
Dans le but d'introduire les investisseurs aux initiateurs des fonds, les sociétés de gestion du "Private Equity" préparent des stratégies spécifiques pour ses clients et trouvent les fonds adéquats à chaque client.

Dans un même temps que l'évolution du marché de gestion des actifs, le monde de la monnaie électronique commence à prouver sa valeur à travers des systèmes tels que Bitcoin. Cela est suivi par des systèmes de gestion des identités virtuelles et de nouveaux genres d'applications distribuées basées sur les techniques Blockchain. Bitcoin, par exemple, atteint une valeur de XX USD en XX-XX-2017.

Les techniques Blockchain poussent les solutions financières innovantes et inhabituelles. C'est un milieu où une infinité de possibilités d'amélioration des solutions actuelles existent.
En offrant des nouvaux paradigmes de confiance et de communication, une transparence des actions, et une stabilité et efficacité technique, le Blockchain vise à améliorer l'état des affaires.

Reconnaissant la puissance de cette technique, les grands acteurs financiers dans le monde ont changé leurs stratégies et ont commencé à étudier cette technique sérieusement, essayant d'extraire ses bénéfices et de trouver des opportunités et des nouvaux marchés.

Talan Consulting, comme l'un des acteurs dans le mode de gestion des actifs et de développment des application financières, commence à faire confience à cette technologie. Comme un membre de l'équipe Blockchain, notre projet consiste à améliorer la procédure du Fundraising mise en place en exploitants les avantages du Blockchain.

**Problématique**

***Présentation du projet***

Dans sa recherche continue à améliorer les procédures actuelles, Talan Consulting cherche à apporter les avantages des techniques Blockchain à son système d'informations de Private Equity actuel. Ce système est l'un des plus grands systèmes de l'entreprise. Pourcela, nous nous intéressons dans notre projet sur l'application qui gère la procédure du "Fundraising" de Private Equity.

Fundraising est l'ensemble des activités et systèmes qui cherchent à des contributions dans des fonds. L'application Fundraising se concentre sur la gestion des opportunités des investisseurs dans les fonds. La gestion de progrès de ces fonds et leurs transformations en des parts dans les fonds sont les principales problème que nous souhaitons résoudre.

Notre mission est d'utiliser les techniques Blockchain pour améliorer la procédure du fundraising existante. La solution sera utilisée par les acteurs actuels et donc devra respecter leurs environnements de travail, leurs contraintes règlementaires, et leus différentes approches de gestion des risques.

***Étude de l’existant***

Il existe, chez l'entreprise, un système de gestion des activités de "Fundraising". C'est un système centralisé avec un seul serveur, qui se trouve chez la sociétés de gestion du "Private Equity", et un ensemble limité de clients. Les clients n'ont pas de visibilité sur les modifications des données et sur les actions effectuées sur le système. Ils fonts, donc, confiance à la sociétés de gestion du "Private Equity" pour respecter les règlementations et l'intégrité des données.

L'adhésion sur ce système a un coût assez élevé. Par suite, chaque transaction coûte l'utilisateur une somme d'argent. Ce sont les relations investisseur-initiateur de fonds et la confiance des clients qui génèrent le revenu cette société.

**Solution proposée**

Notre solution apporte les avanatges suivantes par rapport à la solution existente:

- Diminution du coût de participation
- Distribution de confiance entre les utilisateurs du système
- Transparence sur les actions et les données

**Terminologie**

***Fundraising***

Le Fundraising est la collecte des fonds pour les investir aux capitaux des entités. Ces entité présente souvent de jeunes entreprises dans notre cas.

La société responsable du collecte des fonds et la proposition des offres d'invesstisselent est une société de servies financères, intitulé 'Société de gestion des actifs'.
Cette dernière investe les fonds de ses clients dans les capitaux des autres entités à fin de réaliser des objectifs définis par un accord initial.
Pour cela, ce type de sociétés prépare des stratégies d'investissement pour chaque type de clients et propose un large éventail d'offres qui pourraient ne pas être disponibles pour l'investisseur moyen.

***Blockchain***

Une Blockchain est une séquence de transactions ordonnée dans un ordre chronologique.
Chaque suite de transactions se trouve dans un block distant, d'oú le nom Blockchain(chaine de blocks).
Cette séquence est, finalement, immutable -modifiable que par écriture.

Blockchain se base sur un protocole de consensus mutuel qui préserve la consistence des données dans la chaine et qui réduit la probabilité des attaques**.

Un système Blockchain est un ensemble d'entité distribuées dont chaqu'un:

- possède une copie de la chaine
- valide les blocks (et les transactions dedans) ajoutées les autres parties en se basant sur des contraintes partagées
- synchronise sa copie de la chaine avec les autre parties suite à chaque ajout d'un block à la chaine
- ajoute des transactions dans à la chaine
- collecte d'ensemble de transactions dans un block et ajouter ce dernier à la chaine

***Notions du domaine***

- Opportunité
  Une chance pour un distributeur d'avoir un part dans un fond
- Distributeur
  Une entité qui peut acheter et vendre des opportunités
- Souscripteur
  Une entité qui peut acheter des opportunités
- Souscription
  Un chance pour un souscripteur d'avoir un part dans un fond
- Fond
  Un part dans une ou plusieur entités
- Titre financier
  Un titre représentatif d'une partie du capital d'une société (actions).
- Actif
  élélement de valeur chez une entité
- Blockchain
  Une technologie qui assure l'existent d'un seul état de l'application chez tous les participants
- Participant
  Un utilisateur de l'application
- Transaction
  Un changement de l'état de l'application
- Private Equity
  les titres de participation qui ne sont pas cotées en bourse

**Méthodologie**

***Comparison***

*Processus unifié*

*Agile Software development*

"Agile Software Development" est une approche de developpement des logiciels basée sur les valeurs suivantes:

- Individuals and interactions over processes and tools
- Working software over comprehensive documentation
- Customer collaboration over contract negotiation
- Responding to change over following a plan

"Agile Software Development" définie aussi des principes de développement logiciels.

Pour qu'elle soit concidérée comme Agile, une initiative de developpment doit respecter les valeurs et les principes qui sont définies par "Agile Manifesto".

*eXtreme Programming*

"XP is a lightweight, efficient, low-risk, flexible, predictable, scientific, and fun way to develop software." --XP explained, Kent Beck

*Tableau comparatif*

***Choix***

Nous somme en face d'un risque technique important et nous avons besoin d'un feedback continue sur la solution. Nous avons, aussi, besoin de la  présence des experts métiers avec nous pour avoir leur avis et identifier les défets tôt.

Nous choisissons, donc, de concevoir un processus de travail inspiré par "eXtreme Programming".

Ce processus respecte les valeurs et les principes de l'"Agile manifesto" et du l'"Extreme Programming".

*Processus utilisé*

"The raw materials of our new software development discipline are:

- The story about learning to drive
- The four values—communication, simplicity, feedback, and courage
- The principles
- The four basic activities—coding, testing, listening, and designing

" --XP explained, Kent Beck

Nous respectons les principes de l'"Extreme Programming" dans nos activités quotidiennes et nous implémentons les principes comme suit:

- Communication
  Nous utilisons Google Hangout quotidiennement et nous nous discutons les détails du métier sur demande.
- Simplicité
  Nous essayons de guarder la conception et les activités de développement le plus simple possible.
- Feedback
  Nous implémentons un processus avec des cycles de feedback dans tous les niveaux de dévéloppment:
  - TDD(Test Driven Development): Feedback immédiat sur l'état de l'application. Tous les tests sont lancés après chaque changement (chaque 2 minutes).
  - ATDD(Acceptance Test Driven Development): Nous validons les scénarios de chaque Feature avant de commencer le développment. La validation se fait à travers des conversations avec l'expert métier et des walkthrough sur des maquettes. On traduit, par suite, les scénarios en suites de test et les maquettes en spécification. (chaque 1 ou 2 fois par itération).
  - Des sessions de retrospection personnelles (chaque fin de semaine).
  - Session de test avec le responsable (chaque fin d'itération).
- Courage
  Les technologies que nous utiliserons dans notre projet sont assez jeunes. Ils peuvent être changées à tous moment. L'architecture de l'application, elle aussi, change souvent. Nous sommes besoin du courage pour changer la technologie Blockchain utilisé au cours de développement et faire des modification sur l'architecture de la solution.